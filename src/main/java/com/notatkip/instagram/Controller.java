package com.notatkip.instagram;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jinstagram.Instagram;
import org.jinstagram.auth.InstagramAuthService;
import org.jinstagram.auth.model.Token;
import org.jinstagram.auth.model.Verifier;
import org.jinstagram.auth.oauth.InstagramService;
import org.jinstagram.entity.common.User;
import org.jinstagram.entity.users.feed.MediaFeed;
import org.jinstagram.entity.users.feed.MediaFeedData;
import org.jinstagram.exceptions.InstagramException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    private static final String API_SECRET = "";

    private static final String API_KEY = "79d95c8639ad44ada9785cb14f00eff1";

    private static final String CALLBACK = "http://localhost:8080/redirect";
    
    private static final String SCOPE = "basic+public_content+likes";


    @RequestMapping("/login")
    public String index() {

        InstagramService service = new InstagramAuthService().apiKey(API_KEY).apiSecret(API_SECRET).callback(CALLBACK).scope(SCOPE).build();

        return service.getAuthorizationUrl();
    }


    @RequestMapping("/redirect")
    public String redirect(@RequestParam(value = "code", required = true) String code) throws InstagramException {

    	return retrieveToken(code).getToken();
    }


    @RequestMapping("/giveLikes")
    public String userInfo(@RequestParam(value = "token", required = true) String token, @RequestParam(value = "user", required = false) String user) throws InstagramException {
        if (StringUtils.isBlank(user)) {
            return "user is blank";
        }
        
        
        Instagram instagram = buildInstagram(token);
        String userId = instagram.searchUser(user).getUserList().get(0).getId();
        System.out.println("userId: "+userId);
        String lastImageId = instagram.getRecentMediaFeed(userId).getData().get(0).getId();


        
        List<User> users = instagram.getMediaInfo(lastImageId).getData().getLikes().getLikesUserList();
        
        if(users == null || users.isEmpty()){
        	return "nobody liked photo or something went wrong";
        }
        System.out.println("users to like: "+users.size());
        
        int count = 0;
        for(User u : users){
        	String id = u.getId();
        	MediaFeed mf = instagram.getRecentMediaFeed(id);
        	if(mf == null){
        		continue;
        	}
        	List<MediaFeedData> mediaFeedData = mf.getData();
        	if(mediaFeedData == null || mediaFeedData.isEmpty() || mediaFeedData.get(0).isUserHasLiked()){
        		continue;
        	}

        	instagram.setUserLike(mediaFeedData.get(0).getId());
        	count++;
        	if(count > 100){
        		break;
        	}
        }
        return "gived "+count+" likes";
    }


    private Token retrieveToken(String code) {
        InstagramService service = new InstagramAuthService().apiKey(API_KEY).apiSecret(API_SECRET).callback(CALLBACK)
                .scope(SCOPE).build();

        Verifier verifier = new Verifier(code);
        return service.getAccessToken(verifier);
    }


    private Instagram buildInstagram(String token) {
        return new Instagram(new Token(token, API_SECRET));
    }

}